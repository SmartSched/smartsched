"""
SmartSched
This is a main package of a web-app
"""

from os.path import dirname, realpath

__author__ = 'SmartSched team'
__date__ = '2015-04-17'
__appname__ = 'SmartSched'
__version__ = '0.1'
__config__ = 'config.yaml'

SRC_DIR = dirname(realpath(__file__))
PRJ_ROOT = dirname(dirname(SRC_DIR))
