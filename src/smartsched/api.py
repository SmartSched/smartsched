from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from model import Application, Comment, Course, Group, Lesson, Place, Student, Tag, Teacher

class Api:

    engine = None
    orm_session = None

    def __init__(self, db_config, **kwargs):
        connection_uri = '{driver}://{user}:{password}@{host}:{port}/{db_name}?charset=utf8'.format(
                driver=db_config['driver'],
                user=db_config['user'],
                password=db_config['password'],
                host=db_config['host'],
                port=db_config['port'],
                db_name =db_config['database']
            )
        self.engine = create_engine(connection_uri)
        Session = sessionmaker(bind = self.engine)
        self.orm_session = Session()

    # Tags
    def tag_create(self, new_tag):
        self.orm_session.add(new_tag)
        self.orm_session.commit()

    def tag_delete(self, _id):
        tag = self.orm_session.query(Tag).get(_id)
        self.orm_session.delete(tag)
        self.orm_session.commit()

    def tag_get(self, _id):
        tag = self.orm_session.query(Tag).get(_id)
        return tag 

    def tag_list(self):
        tags = self.orm_session.query(Tag).all()
        return tags

    def tag_update(self, _id, updated_tag):
        updated_tag._id = _id
        tag = self.orm_session.merge(updated_tag)
        self.orm_session.commit()
        return tag

    # Courses
    def course_create(self, new_course):
        self.orm_session.add(new_course)
        self.orm_session.commit()

    def course_delete(self, _id):
        course = self.orm_session.query(Course).get(_id)
        self.orm_session.delete(course)
        self.orm_session.commit()

    def course_get(self, _id):
        course = self.orm_session.query(Course).get(_id)
        return course

    def course_list(self):
        courses = self.orm_session.query(Course).all()
        return courses

    def course_update(self, _id, updated_course):
        updated_course._id = _id
        course = self.orm_session.merge(updated_course)
        self.orm_session.commit()
        return course

    # Teacher
    def teacher_create(self, new_teacher):
        self.orm_session.add(new_teacher)
        self.orm_session.commit()

    def teacher_delete(self, _id):
        teacher = self.orm_session.query(Teacher).get(_id)
        self.orm_session.delete(teacher)
        self.orm_session.commit()

    def teacher_get(self, _id):
        teacher = self.orm_session.query(Teacher).get(_id)
        return teacher

    def teacher_list(self):
        teachers = self.orm_session.query(Teacher).all()
        return teachers

    def teacher_update(self, _id, updated_teacher):
        updated_teacher._id = _id
        teacher = self.orm_session.merge(updated_teacher)
        self.orm_session.commit()
        return teacher

    # Lesson
    def lesson_create(self, new_lesson):
        self.orm_session.add(new_lesson)
        self.orm_session.commit()

    def lesson_delete(self, _id):
        lesson = self.orm_session.query(Lesson).get(_id)
        self.orm_session.delete(lesson)
        self.orm_session.commit()

    def lesson_get(self, _id):
        lesson = self.orm_session.query(Lesson).get(_id)
        return lesson

    def lesson_list(self):
        lessons = self.orm_session.query(Lesson).all()
        return lessons

    def lesson_update(self, _id, updated_lesson):
        updated_lesson._id = _id
        lesson = self.orm_session.merge(updated_lesson)
        self.orm_session.commit()
        return lesson
    
    # Place
    def place_create(self, new_place):
        self.orm_session.add(new_place)
        self.orm_session.commit()

    def place_delete(self, _id):
        place = self.orm_session.query(Place).get(_id)
        self.orm_session.delete(place)
        self.orm_session.commit()

    def place_get(self, _id):
        place = self.orm_session.query(Place).get(_id)
        return place

    def place_list(self):
        places = self.orm_session.query(Place).all()
        return places

    def place_update(self, _id, updated_place):
        updated_place._id = _id
        place = self.orm_session.merge(updated_place)
        self.orm_session.commit()
        return place
    
    # Student
    def student_create(self, new_student):
        self.orm_session.add(new_student)
        self.orm_session.commit()

    def student_delete(self, _id):
        student = self.orm_session.query(Student).get(_id)
        self.orm_session.delete(student)
        self.orm_session.commit()

    def student_get(self, _id):
        student = self.orm_session.query(Student).get(_id)
        return student

    def student_list(self):
        students = self.orm_session.query(Student).all()
        return students

    def student_update(self, _id, updated_student):
        updated_student._id = _id
        student = self.orm_session.merge(updated_student)
        self.orm_session.commit()
        return student
    
    # Group
    def group_create(self, new_group):
        self.orm_session.add(new_group)
        self.orm_session.commit()

    def group_delete(self, _id):
        group = self.orm_session.query(Group).get(_id)
        self.orm_session.delete(group)
        self.orm_session.commit()

    def group_get(self, _id):
        group = self.orm_session.query(Group).get(_id)
        return group

    def group_list(self):
        groups = self.orm_session.query(Group).all()
        return groups

    def group_update(self, _id, updated_group):
        updated_group._id = _id
        group = self.orm_session.merge(updated_group)
        self.orm_session.commit()
        return group
    
    # Comment
    def comment_create(self, new_comment):
        self.orm_session.add(new_comment)
        self.orm_session.commit()

    def comment_delete(self, _id):
        comment = self.orm_session.query(Comment).get(_id)
        self.orm_session.delete(comment)
        self.orm_session.commit()

    def comment_get(self, _id):
        comment = self.orm_session.query(Comment).get(_id)
        return comment

    def comment_list(self):
        comments = self.orm_session.query(Comment).all()
        return comments

    def comment_update(self, _id, updated_comment):
        updated_comment._id = _id
        comment = self.orm_session.merge(updated_comment)
        self.orm_session.commit()
        return comment
    
    # Application
    def application_create(self, new_application):
        self.orm_session.add(new_application)
        self.orm_session.commit()

    def application_delete(self, _id):
        application = self.orm_session.query(Application).get(_id)
        self.orm_session.delete(application)
        self.orm_session.commit()

    def application_get(self, _id):
        application = self.orm_session.query(Application).get(_id)
        return application

    def application_list(self):
        applications = self.orm_session.query(Application).all()
        return applications

    def application_update(self, _id, updated_application):
        updated_application._id = _id
        application = self.orm_session.merge(updated_application)
        self.orm_session.commit()
        return application

