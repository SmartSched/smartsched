from flask import Flask, request
from smartsched import SRC_DIR, PRJ_ROOT, __appname__, __config__
import os
import yaml
from api import Api
from rest_controller import *

class Application():
    
    flask_app = None

    host = None    
    port = None

    api = None
    tag_controller = None
    course_controller = None
    teacher_controller = None
    lesson_controller = None
    place_controller = None
    student_controller = None
    application_controller = None
    group_controller = None
    comment_controller = None

    def __init__(self, config):
        # read hot and port:
        self.host = config['server']['host']
        self.port = config['server']['port']

        # todo: read static dir path:

        self.flask_app = Flask(__appname__)        
       
        self.api = Api(config['db_connection'])

        # rest controllers
        self.tag_controller = TagRestController(self.api)
        self.flask_app.add_url_rule('/api/tags/', methods=['GET'], view_func=self.tag_controller.get_list, endpoint='tag_list')
        self.flask_app.add_url_rule('/api/tags/<int:_id>', methods=['GET'], view_func=self.tag_controller.get, endpoint='tag_get')
        self.flask_app.add_url_rule('/api/tags/<int:_id>', methods=['DELETE'], view_func=self.tag_controller.delete, endpoint='tag_delete')  
        self.flask_app.add_url_rule('/api/tags/', methods=['POST'], view_func=self.tag_controller.create, endpoint='tag_create')
        self.flask_app.add_url_rule('/api/tags/<int:_id>', methods=['PATCH'], view_func=self.tag_controller.update, endpoint='tag_update')
        
        self.course_controller = CourseRestController(self.api)
        self.flask_app.add_url_rule('/api/courses/', methods=['GET'], view_func=self.course_controller.get_list, endpoint='course_list')
        self.flask_app.add_url_rule('/api/courses/<int:_id>', methods=['GET'], view_func=self.course_controller.get, endpoint='course_get')
        self.flask_app.add_url_rule('/api/courses/<int:_id>', methods=['DELETE'], view_func=self.course_controller.delete, endpoint='course_delete')  
        self.flask_app.add_url_rule('/api/courses/', methods=['POST'], view_func=self.course_controller.create, endpoint='course_create')
        self.flask_app.add_url_rule('/api/courses/<int:_id>', methods=['PATCH'], view_func=self.course_controller.update, endpoint='course_update')

        self.teacher_controller = TeacherRestController(self.api)
        self.flask_app.add_url_rule('/api/teachers/', methods=['GET'], view_func=self.teacher_controller.get_list, endpoint='teacher_list')
        self.flask_app.add_url_rule('/api/teachers/<int:_id>', methods=['GET'], view_func=self.teacher_controller.get, endpoint='teacher_get')
        self.flask_app.add_url_rule('/api/teachers/<int:_id>', methods=['DELETE'], view_func=self.teacher_controller.delete, endpoint='teacher_delete')  
        self.flask_app.add_url_rule('/api/teachers/', methods=['POST'], view_func=self.teacher_controller.create, endpoint='teacher_create')
        self.flask_app.add_url_rule('/api/teachers/<int:_id>', methods=['PATCH'], view_func=self.teacher_controller.update, endpoint='teacher_update')

        self.lesson_controller = LessonRestController(self.api)
        self.flask_app.add_url_rule('/api/lessons/', methods=['GET'], view_func=self.lesson_controller.get_list, endpoint='lesson_list')
        self.flask_app.add_url_rule('/api/lessons/<int:_id>', methods=['GET'], view_func=self.lesson_controller.get, endpoint='lesson_get')
        self.flask_app.add_url_rule('/api/lessons/<int:_id>', methods=['DELETE'], view_func=self.lesson_controller.delete, endpoint='lesson_delete')  
        self.flask_app.add_url_rule('/api/lessons/', methods=['POST'], view_func=self.lesson_controller.create, endpoint='lesson_create')
        self.flask_app.add_url_rule('/api/lessons/<int:_id>', methods=['PATCH'], view_func=self.lesson_controller.update, endpoint='lesson_update')

        self.place_controller = PlaceRestController(self.api)
        self.flask_app.add_url_rule('/api/places/', methods=['GET'], view_func=self.place_controller.get_list, endpoint='place_list')
        self.flask_app.add_url_rule('/api/places/<int:_id>', methods=['GET'], view_func=self.place_controller.get, endpoint='place_get')
        self.flask_app.add_url_rule('/api/places/<int:_id>', methods=['DELETE'], view_func=self.place_controller.delete, endpoint='place_delete')  
        self.flask_app.add_url_rule('/api/places/', methods=['POST'], view_func=self.place_controller.create, endpoint='place_create')
        self.flask_app.add_url_rule('/api/places/<int:_id>', methods=['PATCH'], view_func=self.place_controller.update, endpoint='place_update')
        
        self.student_controller = StudentRestController(self.api)
        self.flask_app.add_url_rule('/api/students/', methods=['GET'], view_func=self.student_controller.get_list, endpoint='student_list')
        self.flask_app.add_url_rule('/api/students/<int:_id>', methods=['GET'], view_func=self.student_controller.get, endpoint='student_get')
        self.flask_app.add_url_rule('/api/students/<int:_id>', methods=['DELETE'], view_func=self.student_controller.delete, endpoint='student_delete')  
        self.flask_app.add_url_rule('/api/students/', methods=['POST'], view_func=self.student_controller.create, endpoint='student_create')
        self.flask_app.add_url_rule('/api/students/<int:_id>', methods=['PATCH'], view_func=self.student_controller.update, endpoint='student_update')

        self.application_controller = ApplicationRestController(self.api)
        self.flask_app.add_url_rule('/api/applications/', methods=['GET'], view_func=self.application_controller.get_list, endpoint='application_list')
        self.flask_app.add_url_rule('/api/applications/<int:_id>', methods=['GET'], view_func=self.application_controller.get, endpoint='application_get')
        self.flask_app.add_url_rule('/api/applications/<int:_id>', methods=['DELETE'], view_func=self.application_controller.delete, endpoint='application_delete')  
        self.flask_app.add_url_rule('/api/applications/', methods=['POST'], view_func=self.application_controller.create, endpoint='application_create')
        self.flask_app.add_url_rule('/api/applications/<int:_id>', methods=['PATCH'], view_func=self.application_controller.update, endpoint='application_update')

        self.group_controller = GroupRestController(self.api)
        self.flask_app.add_url_rule('/api/groups/', methods=['GET'], view_func=self.group_controller.get_list, endpoint='group_list')
        self.flask_app.add_url_rule('/api/groups/<int:_id>', methods=['GET'], view_func=self.group_controller.get, endpoint='group_get')
        self.flask_app.add_url_rule('/api/groups/<int:_id>', methods=['DELETE'], view_func=self.group_controller.delete, endpoint='group_delete')  
        self.flask_app.add_url_rule('/api/groups/', methods=['POST'], view_func=self.group_controller.create, endpoint='group_create')
        self.flask_app.add_url_rule('/api/groups/<int:_id>', methods=['PATCH'], view_func=self.group_controller.update, endpoint='group_update')

        self.comment_controller = CommentRestController(self.api)
        self.flask_app.add_url_rule('/api/comments/', methods=['GET'], view_func=self.comment_controller.get_list, endpoint='comment_list')
        self.flask_app.add_url_rule('/api/comments/<int:_id>', methods=['GET'], view_func=self.comment_controller.get, endpoint='comment_get')
        self.flask_app.add_url_rule('/api/comments/<int:_id>', methods=['DELETE'], view_func=self.comment_controller.delete, endpoint='comment_delete')  
        self.flask_app.add_url_rule('/api/comments/', methods=['POST'], view_func=self.comment_controller.create, endpoint='comment_create')
        self.flask_app.add_url_rule('/api/comments/<int:_id>', methods=['PATCH'], view_func=self.comment_controller.update, endpoint='comment_update')




    def run(self, host=None, port=None, debug=None, **options):
        if host == None:
            host = self.host
        if port == None:
            port = self.port
        self.flask_app.run(host=host, port=port, debug=debug, **options)


def main():
    config = None
    cfg_file_path = os.path.join(PRJ_ROOT, 'config', __config__)

    try:
        with open(cfg_file_path, 'r') as f:
            config = yaml.load(f)
        #todo: check if config contains all required sections:
    except IOError:
        print('Invalid or missing config file: {}'.format(cfg_file_path))
    except KeyError:
        print('No configuration found')
        sys.exit(1)
    except Exception as ex:
        print('Something unexpected happened')
        raise ex

    # replace db config with environmental variables, if exist
    db_drvr = os.environ.get('DB_DRVR')
    if db_drvr != None:
        config['db_connection']['driver'] = db_drvr
    
    db_user = os.environ.get('DB_USER')
    if db_user != None:
        config['db_connection']['user'] = db_user

    db_pass = os.environ.get('DB_PASS')
    if db_pass != None:
        config['db_connection']['password'] = db_pass
    
    db_host = os.environ.get('DB_HOST')
    if db_host != None:
        config['db_connection']['host'] = db_host

    db_port = os.environ.get('DB_PORT')
    if db_port != None:
        config['db_connection']['port'] = db_port

    db_name = os.environ.get('DB_NAME')
    if db_name != None:
        config['db_connection']['database'] = db_name

    global app
    app = Application(config)
    app.run(debug=True)

if __name__ == '__main__':
    main()

