from model import *
from application import Application
from flask import request
import datetime
import json

class BaseRestController:

    api = None
    
    def __init__(self, api, **kwargs):
        self.api = api


class TagRestController(BaseRestController):

    def __init__(self, api, **kwargs):
        BaseRestController.__init__(self, api)
         
    def create(self):
        tag = Tag()
        tag.tag_name = request.form['tag_name']
        self.api.tag_create(tag)
        return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

    def delete(self, _id):
        self.api.tag_delete(_id)
        return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

    def get(self, _id):
        tag = self.api.tag_get(_id)
        response = json.dumps(tag, cls=new_alchemy_encoder(), check_circular=False, ensure_ascii=False)
        return response

    def get_list(self):
        tags = self.api.tag_list()
        response = json.dumps(tags, cls=new_alchemy_encoder(), check_circular=False, ensure_ascii=False)
        return response
    
    def update(self, _id):
        tag = Tag()
        tag.tag_name = request.form['tag_name']
        self.api.tag_update(_id, tag)
        return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

       
class CourseRestController(BaseRestController):
    
    def __init__(self, api, **kwargs):
        BaseRestController.__init__(self, api)

    def create(self):
        course = Course()
        course.course_title = request.form['course_title']
        course.teacher_id = request.form['teacher_id']
        tag_ids = json.loads(request.form['tag_ids'])
        for tag_id in tag_ids:
            tag = self.api.tag_get(tag_id)
            course.tags.append(tag)
        self.api.course_create(course)
        return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

    def delete(self, _id):
        self.api.course_delete(_id)
        return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

    def get(self, _id):
        course = self.api.course_get(_id)
        response = json.dumps(course, cls=new_alchemy_encoder(), check_circular=False, ensure_ascii=False)
        return response

    def get_list(self):
        courses = self.api.course_list()
        response = json.dumps(courses, cls=new_alchemy_encoder(), check_circular=False, ensure_ascii=False)
        return response

    def update(self, _id):
        course = Course()
        course.course_title = request.form['course_title']
        course.teacher_id = request.form['teacher_id']
        tag_ids = json.loads(request.form['tag_ids'])
        for tag_id in tag_ids:
            tag = self.api.tag_get(tag_id)
            course.tags.append(tag)       
        self.api.course_update(_id, course)
        return json.dumps({'success':True}), 200, {'ContentType':'application/json'}
       

class TeacherRestController(BaseRestController):
    
    def __init__(self, api, **kwargs):
        BaseRestController.__init__(self, api)

    def create(self):
        teacher = Teacher()
        teacher.first_name = request.form['first_name']
        teacher.last_name = request.form['last_name']
        self.api.teacher_create(teacher)
        return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

    def delete(self, _id):
        self.api.teacher_delete(_id)
        return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

    def get(self, _id):
        teacher = self.api.teacher_get(_id)
        response = json.dumps(teacher, cls=new_alchemy_encoder(), check_circular=False, ensure_ascii=False)
        return response

    def get_list(self):
        teachers = self.api.teacher_list()
        response = json.dumps(teachers, cls=new_alchemy_encoder(), check_circular=False, ensure_ascii=False)
        return response

    def update(self, _id):
        teacher = Teacher()
        teacher.first_name = request.form['first_name']
        teacher.last_name = request.form['last_name']
        self.api.teacher_update(_id, teacher)
        return json.dumps({'success':True}), 200, {'ContentType':'application/json'}


class LessonRestController(BaseRestController):
    
    def __init__(self, api, **kwargs):
        BaseRestController.__init__(self, api)

    def create(self):
        lesson = Lesson()
        lesson.lesson_date = datetime.datetime.strptime(request.form['lesson_date'], '%Y-%m-%d %H:%M:%S')
        lesson.lesson_num = request.form['lesson_num']
        lesson.lesson_duration = request.form['lesson_duration']
        lesson.place_id = request.form['place_id']
        lesson.teacher_id = request.form['teacher_id']
        self.api.lesson_create(lesson)
        return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

    def delete(self, _id):
        self.api.lesson_delete(_id)
        return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

    def get(self, _id):
        lesson = self.api.lesson_get(_id)
        response = json.dumps(lesson, cls=new_alchemy_encoder(), check_circular=False, ensure_ascii=False)
        return response

    def get_list(self):
        lessons = self.api.lesson_list()
        response = json.dumps(lessons, cls=new_alchemy_encoder(), check_circular=False, ensure_ascii=False)
        return response

    def update(self, _id):
        lesson = Lesson()
        lesson.lesson_date = datetime.datetime.strptime(request.form['lesson_date'], '%Y-%m-%d %H:%M:%S')
        lesson.lesson_num = request.form['lesson_num']
        lesson.lesson_duration = request.form['lesson_duration']
        lesson.place_id = request.form['place_id']
        lesson.teacher_id = request.form['teacher_id']
        self.api.lesson_update(_id, lesson)
        return json.dumps({'success':True}), 200, {'ContentType':'application/json'}


class PlaceRestController(BaseRestController):
    
    def __init__(self, api, **kwargs):
        BaseRestController.__init__(self, api)

    def create(self):
        place = Place()
        place.building = request.form['building']
        place.classroom = request.form['classroom']
        self.api.place_create(place)
        return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

    def delete(self, _id):
        self.api.place_delete(_id)
        return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

    def get(self, _id):
        place = self.api.place_get(_id)
        response = json.dumps(place, cls=new_alchemy_encoder(), check_circular=False, ensure_ascii=False)
        return response

    def get_list(self):
        places = self.api.place_list()
        response = json.dumps(places, cls=new_alchemy_encoder(), check_circular=False, ensure_ascii=False)
        return response

    def update(self, _id):
        place = Place()
        place.building = request.form['building']
        place.classroom = request.form['classroom']
        self.api.place_update(_id, place)
        return json.dumps({'success':True}), 200, {'ContentType':'application/json'}
       

class StudentRestController(BaseRestController):
    
    def __init__(self, api, **kwargs):
        BaseRestController.__init__(self, api)

    def create(self):
        student = Student()
        student.first_name = request.form['first_name']
        student.last_name = request.form['last_name']
        student.e_mail = request.form['e_mail']
        self.api.student_create(student)
        return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

    def delete(self, _id):
        self.api.student_delete(_id)
        return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

    def get(self, _id):
        student = self.api.student_get(_id)
        response = json.dumps(student, cls=new_alchemy_encoder(), check_circular=False, ensure_ascii=False)
        return response

    def get_list(self):
        students = self.api.student_list()
        response = json.dumps(students, cls=new_alchemy_encoder(), check_circular=False, ensure_ascii=False)
        return response

    def update(self, _id):
        student = Student()
        student.first_name = request.form['first_name']
        student.last_name = request.form['last_name']
        student.e_mail = request.form['e_mail']
        self.api.student_update(_id, student)
        return json.dumps({'success':True}), 200, {'ContentType':'application/json'}
       

class ApplicationRestController(BaseRestController):
    
    def __init__(self, api, **kwargs):
        BaseRestController.__init__(self, api)

    def create(self):
        application = Application()
        application.course_id = request.form['course_id']
        application.group_id = request.form['group_id']
        application.student_id = request.form['student_id']
        self.api.application_create(application)
        return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

    def delete(self, _id):
        self.api.application_delete(_id)
        return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

    def get(self, _id):
        application = self.api.application_get(_id)
        response = json.dumps(application, cls=new_alchemy_encoder(), check_circular=False, ensure_ascii=False)
        return response

    def get_list(self):
        applications = self.api.application_list()
        response = json.dumps(applications, cls=new_alchemy_encoder(), check_circular=False, ensure_ascii=False)
        return response

    def update(self, _id):
        application = Application()
        application.course_id = request.form['course_id']
        application.group_id = request.form['group_id']
        application.student_id = request.form['student_id']
        self.api.application_update(_id, application)
        return json.dumps({'success':True}), 200, {'ContentType':'application/json'}
       

class GroupRestController(BaseRestController):
    
    def __init__(self, api, **kwargs):
        BaseRestController.__init__(self, api)

    def create(self):
        group = Group()
        group.group_title = request.form['group_title']
        group.lesson_id = request.form['lesson_id']
        group.group_id = request.form['group_id']
        self.api.group_create(group)
        return json.dumps({'success':True}), 200, {'ContentType':'group/json'}

    def delete(self, _id):
        self.api.group_delete(_id)
        return json.dumps({'success':True}), 200, {'ContentType':'group/json'}

    def get(self, _id):
        group = self.api.group_get(_id)
        response = json.dumps(group, cls=new_alchemy_encoder(), check_circular=False, ensure_ascii=False)
        return response

    def get_list(self):
        groups = self.api.group_list()
        response = json.dumps(groups, cls=new_alchemy_encoder(), check_circular=False, ensure_ascii=False)
        return response

    def update(self, _id):
        group = Group()
        group.group_title = request.form['group_title']
        group.lesson_id = request.form['lesson_id']
        group.group_id = request.form['group_id']
        self.api.group_update(_id, group)
        return json.dumps({'success':True}), 200, {'ContentType':'group/json'}
       

class CommentRestController(BaseRestController):
    
    def __init__(self, api, **kwargs):
        BaseRestController.__init__(self, api)

    def create(self):
        comment = Comment()
        comment.comment_text = request.form['comment_text']
        comment.comment_rating = request.form['comment_rating']
        comment.student_id = request.form['student_id']
        comment.course_id = request.form['course_id']
        comment.lesson_id = request.form['lesson_id']
        self.api.comment_create(comment)
        return json.dumps({'success':True}), 200, {'ContentType':'comment/json'}

    def delete(self, _id):
        self.api.comment_delete(_id)
        return json.dumps({'success':True}), 200, {'ContentType':'comment/json'}

    def get(self, _id):
        comment = self.api.comment_get(_id)
        response = json.dumps(comment, cls=new_alchemy_encoder(), check_circular=False, ensure_ascii=False)
        return response

    def get_list(self):
        comments = self.api.comment_list()
        response = json.dumps(comments, cls=new_alchemy_encoder(), check_circular=False, ensure_ascii=False)
        return response

    def update(self, _id):
        comment = Comment()
        comment.comment_text = request.form['comment_text']
        comment.comment_rating = request.form['comment_rating']
        comment.student_id = request.form['student_id']
        comment.course_id = request.form['course_id']
        comment.lesson_id = request.form['lesson_id']
        self.api.comment_update(_id, comment)
        return json.dumps({'success':True}), 200, {'ContentType':'comment/json'}
       

