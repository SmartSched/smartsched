from sqlalchemy import Column, ForeignKey, Integer, String, DateTime, SmallInteger, Table
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

from sqlalchemy.ext.declarative import DeclarativeMeta
import datetime
import json

Base = declarative_base()

def new_alchemy_encoder():
    class AlchemyEncoder(json.JSONEncoder):
        def default(self, obj):
            if isinstance(obj, datetime.datetime):
                return str(obj)
            elif isinstance(obj.__class__, DeclarativeMeta):

                # an SQLAlchemy class
                fields = {}
                for field in [x for x in dir(obj) if x == '_id' or (not (x.startswith('_') or x.startswith('br_') or x.endswith('_id')) and x != 'metadata')]:
                    fields[field] = obj.__getattribute__(field)
                # a json-encodable dict
                return fields

            return json.JSONEncoder.default(self, obj)
    return AlchemyEncoder

class Tag(Base):

    def __init__(self, **kwargs):
        super(Tag, self).__init__(**kwargs)

    __tablename__ = 'tags'
    _id = Column(Integer, primary_key=True)
    tag_name = Column(String(127, convert_unicode=True), nullable=False)


linking_tag_course_table = Table(
    'linking_tag_course', 
    Base.metadata,
    Column('tag_id', Integer, ForeignKey('tags._id')),
    Column('course_id', Integer, ForeignKey('courses._id'))
)


class Course(Base):
 
    def __init__(self, **kwargs):
        super(Course, self).__init__(**kwargs)

    __tablename__ = 'courses'
    _id = Column(Integer, primary_key=True)
    course_title = Column(String(255))
    teacher_id = Column(Integer, ForeignKey('teachers._id'))
    tags = relationship("Tag", secondary=linking_tag_course_table, backref="br_course")
    teacher = relationship("Teacher", backref="br_teacher")


class Teacher(Base):
 
    def __init__(self, **kwargs):
        super(Teacher, self).__init__(**kwargs)

    __tablename__ = 'teachers'
    _id = Column(Integer, primary_key=True)
    first_name = Column(String(127))
    last_name = Column(String(127))


class Lesson(Base):
 
    def __init__(self, **kwargs):
        super(Lesson, self).__init__(**kwargs)

    __tablename__ = 'lessons'
    _id = Column(Integer, primary_key=True)
    lesson_date = Column(DateTime)
    lesson_num = Column(Integer)
    lesson_duration = Column(Integer)
    place_id = Column(Integer, ForeignKey('places._id'), nullable=False)
    teacher_id = Column(Integer, ForeignKey('teachers._id'), nullable=False)
    place = relationship("Place", backref="br_lesson")
    teacher = relationship ("Teacher", backref="br_lesson")


class Place(Base):
 
    def __init__(self, **kwargs):
        super(Place, self).__init__(**kwargs)

    __tablename__ = 'places'
    _id = Column(Integer, primary_key=True)
    building = Column(Integer)
    classroom = Column(Integer)


class Student(Base):
 
    def __init__(self, **kwargs):
        super(Student, self).__init__(**kwargs)

    __tablename__ = 'students'
    _id = Column(Integer, primary_key=True)
    first_name = Column(String(127))
    last_name = Column(String(127))
    e_mail = Column(String(255))


class Application(Base):
 
    def __init__(self, **kwargs):
        super(Application, self).__init__(**kwargs)

    __tablename__ = 'applications'
    _id = Column(Integer, primary_key=True)
    course_id = Column(Integer, ForeignKey('courses._id'), nullable=False)
    group_id = Column(Integer, ForeignKey('groups._id'))
    student_id = Column(Integer, ForeignKey('students._id'), nullable=False)
    course = relationship("Course", backref="br_application")
    group = relationship("Group", backref="br_application")
    student = relationship("Student", backref="br_application")


class Group(Base):
 
    def __init__(self, **kwargs):
        super(Group, self).__init__(**kwargs)

    __tablename__ = 'groups'
    _id = Column(Integer, primary_key=True)
    group_title = Column(String(64))
    lesson_id = Column(Integer, ForeignKey('lessons._id'))
    group_id = Column(Integer, ForeignKey('groups._id'))
    lesson = relationship("Lesson", backref="br_group")


class Comment(Base):
 
    def __init__(self, **kwargs):
        super(Comment, self).__init__(**kwargs)

    __tablename__ = 'comments'
    _id = Column(Integer, primary_key=True)
    comment_text = Column(String(8192))
    comment_rating = Column(SmallInteger)
    student_id = Column(Integer, ForeignKey('students._id'), nullable=False)
    course_id = Column(Integer, ForeignKey('courses._id'), nullable=False)
    lesson_id = Column(Integer, ForeignKey('lessons._id'))
    student = relationship("Student", backref="br_comment")
    course = relationship("Course", backref="br_comment")
    lesson = relationship("Lesson", backref="br_comment")


