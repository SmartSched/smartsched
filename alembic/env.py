from __future__ import with_statement
from alembic import context
from sqlalchemy import create_engine
from smartsched import PRJ_ROOT, __config__
import yaml
import os



config = None
cfg_file_path = os.path.join(PRJ_ROOT, 'config', __config__)
try:
    with open(cfg_file_path, 'r') as f:
        config = yaml.load(f)
    #todo: check if config contains all required sections:
except IOError:
    print('Invalid or missing config file: {}'.format(cfg_file_path))
except KeyError:
    print('No configuration found')
    sys.exit(1)
except Exception as ex:
    print('Something unexpected happened')
    raise ex

# replace db config with environmental variables, if exist
db_drvr = os.environ.get('DB_DRVR')
if db_drvr != None:
    config['db_connection']['driver'] = db_drvr

db_user = os.environ.get('DB_USER')
if db_user != None:
    config['db_connection']['user'] = db_user

db_pass = os.environ.get('DB_PASS')
if db_pass != None:
    config['db_connection']['password'] = db_pass

db_host = os.environ.get('DB_HOST')
if db_host != None:
    config['db_connection']['host'] = db_host

db_port = os.environ.get('DB_PORT')
if db_port != None:
    config['db_connection']['port'] = db_port

db_name = os.environ.get('DB_NAME')
if db_name != None:
    config['db_connection']['database'] = db_name

db_config = config['db_connection']

# add your model's MetaData object here
# for 'autogenerate' support
from smartsched import model as mymodel
target_metadata = mymodel.Base.metadata

# other values from the config, defined by the needs of env.py,
# can be acquired:
# my_important_option = config.get_main_option("my_important_option")
# ... etc.


def run_migrations_offline():
    """Run migrations in 'offline' mode.

    This configures the context with just a URL
    and not an Engine, though an Engine is acceptable
    here as well.  By skipping the Engine creation
    we don't even need a DBAPI to be available.

    Calls to context.execute() here emit the given string to the
    script output.

    """

    connection_uri = '{driver}://{user}:{password}@{host}:{port}/{db_name}?charset=utf8'.format(
                driver=db_config['driver'],
                user=db_config['user'],
                password=db_config['password'],
                host=db_config['host'],
                port=db_config['port'],
                db_name =db_config['database']
            )
    url = connection_uri
    context.configure(
        url=url, target_metadata=target_metadata, literal_binds=True)

    with context.begin_transaction():
        context.run_migrations()


def run_migrations_online():
    """Run migrations in 'online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.

    """
    connection_uri = '{driver}://{user}:{password}@{host}:{port}/{db_name}?charset=utf8'.format(
                driver=db_config['driver'],
                user=db_config['user'],
                password=db_config['password'],
                host=db_config['host'],
                port=db_config['port'],
                db_name =db_config['database']
            ) 
    connectable = create_engine(connection_uri)

    with connectable.connect() as connection:
        context.configure(
            connection=connection,
            target_metadata=target_metadata
        )

        with context.begin_transaction():
            context.run_migrations()

if context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()
