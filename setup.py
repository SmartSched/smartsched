from setuptools import setup, find_packages
import sys

install_requires = ['PyYAML', 'SQLAlchemy', 'Flask']

setup(name='smartsched',
      version='0.0.1',
      author='SmartSched',
      package_dir={'':'src'},
      packages=find_packages('src', exclude=["test**"]),
      install_requires=install_requires,
      zip_safe=False
)
