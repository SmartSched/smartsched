#smartsched

```bash
$ git clone git@bitbucket.org:SmartSched/smartsched.git
$ cd smartsched
$ virtualenv -p python3.4 .env
$ source .env/bin/activate
(.env) $ pip install -r requirements.txt
(.env) $ vim config/config.yaml # fill the db_connection section, create corresponding database in the mysql shell
(.env) $ python src/smartsched/application.py
```

You can send requests to localhost:8080/api/

There some endpoints you can use:
- /tags/
- /courses/
- /teachers/
- /students/
- /lessons/
- /applications/
- /places/
- /comments/
- /groups/